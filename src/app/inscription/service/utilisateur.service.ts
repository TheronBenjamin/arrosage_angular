import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, tap} from "rxjs";
import {Utilisateur} from "../../core/model/utilisateur";
import {Router} from "@angular/router";
import {environment} from "../../../environments/environment";
import {BearerToken} from "../../core/model/bearerToken";

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {

  // url: string = 'http://localhost:3000/utilisateur';
  private url: string = environment.apiURL+ "users/";


  constructor(
    private httpClient : HttpClient,
    private router : Router
  ) { }

  createUser(utilisateur : Utilisateur):Observable<BearerToken>{
    return this.httpClient.post<BearerToken>(this.url + "register", utilisateur)
      .pipe(tap(() => this.router.navigate(['/'])));
  }

}
