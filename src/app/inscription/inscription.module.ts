import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InscriptionRoutingModule } from './inscription-routing.module';
import { InscriptionPageComponent } from './pages/inscription-page/inscription-page.component';
import {MatCardModule} from "@angular/material/card";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import { CguRgpdComponent } from './pages/cgu-rgpd/cgu-rgpd.component';


@NgModule({
  declarations: [
    InscriptionPageComponent,
    CguRgpdComponent,
  ],
    imports: [
        CommonModule,
        InscriptionRoutingModule,
        MatCardModule,
        MatFormFieldModule,
        MatButtonModule,
        MatInputModule,
        FormsModule,
        ReactiveFormsModule,
        MatIconModule
    ]
})
export class InscriptionModule { }
