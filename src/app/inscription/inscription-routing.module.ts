import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {InscriptionPageComponent} from "./pages/inscription-page/inscription-page.component";
import {CguRgpdComponent} from "./pages/cgu-rgpd/cgu-rgpd.component";

const routes: Routes = [
  {path: 'inscription', pathMatch: 'full', component: InscriptionPageComponent},
  {path: 'cgu-rgpd', pathMatch: "full", component: CguRgpdComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InscriptionRoutingModule { }
