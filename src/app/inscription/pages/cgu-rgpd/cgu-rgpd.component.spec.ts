import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CguRgpdComponent } from './cgu-rgpd.component';

describe('CguRgpdComponent', () => {
  let component: CguRgpdComponent;
  let fixture: ComponentFixture<CguRgpdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CguRgpdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CguRgpdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
