import { Component, OnInit } from '@angular/core';
import {UtilisateurService} from "../../service/utilisateur.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import gsap from "gsap";
import {Router} from "@angular/router";

@Component({
  selector: 'app-inscription-page',
  templateUrl: './inscription-page.component.html',
  styleUrls: ['./inscription-page.component.scss']
})
export class InscriptionPageComponent implements OnInit {

  form !:FormGroup;
  key: string = "Authorization";
  errorMessage = "";
  hidePassword: boolean = true;

  constructor(
    fb : FormBuilder,
    private utilisateurService: UtilisateurService,
    private router: Router
    ) {
    this.form = fb.group({
      mail: fb.control(null, [Validators.required, Validators.email]),
      login: fb.control(null, [Validators.required]),
      password: fb.control(null, [Validators.required]),
    })
  }

  ngOnInit(): void {
    gsap.from(".example-card",  {duration:'.7', bottom:'120', ease:"power4"});
    gsap.from(".img",  {duration:'1.6', x:'0', rotation:'360', ease:"back"});
    gsap.from(".example-full-width",  {duration:'1.5', opacity:'0', left:'100', ease:"power4"});
    gsap.from(".account-button",  {duration:'1.5', right:'100', ease:"power4"});
    gsap.from(".create-account-text",  {duration:'1.5', x:'100',opacity:'10', ease:"power4"});
    gsap.from(".title",  {duration:'2',y:'-80', opacity:'0', ease:"power4"});
  }

  submit(){
    this.utilisateurService.createUser(this.form.value).subscribe(
      {
        next: (registerResponse) =>  {
          localStorage.setItem(this.key, 'Bearer ' + registerResponse.token);
          this.errorMessage ="";
          this.router.navigate(['/'])
        },
        error: error => {
          console.log(error);
          this.errorMessage = "Utilisateur déjà existant, modifiez votre adresse mail"
        }
    });
  }
}
