import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { IconAddressCardComponent } from "./icon-address-card/icon-address-card.component";
import { IconCartShoppingComponent } from './icon-cart-shopping/icon-cart-shopping.component';
import { IconLogoutComponent } from './icon-logout/icon-logout.component';
import { IconVegetableComponent } from './icon-vegetable/icon-vegetable.component';




@NgModule({
  declarations: [
    IconAddressCardComponent,
    IconCartShoppingComponent,
    IconLogoutComponent,
    IconVegetableComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule
  ],
  exports: [
    IconAddressCardComponent,
    IconCartShoppingComponent,
    IconLogoutComponent,
    IconVegetableComponent
  ]
})
export class IconsModule { }
