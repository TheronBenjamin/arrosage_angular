import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IconVegetableComponent } from './icon-vegetable.component';

describe('IconVegetableComponent', () => {
  let component: IconVegetableComponent;
  let fixture: ComponentFixture<IconVegetableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IconVegetableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IconVegetableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
