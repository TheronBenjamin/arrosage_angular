import { Component, OnInit } from '@angular/core';
import {IconDefinition} from "@fortawesome/free-regular-svg-icons";
import {faPlantWilt} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-icon-vegetable',
  templateUrl: './icon-vegetable.component.html',
  styleUrls: ['./icon-vegetable.component.scss']
})
export class IconVegetableComponent implements OnInit {

  public myIcon: IconDefinition = faPlantWilt
  constructor() { }

  ngOnInit(): void {
  }

}
