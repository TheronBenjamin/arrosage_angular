import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu/menu.component';
import {RouterModule} from "@angular/router";
import {IconsModule} from "../icons/icons.module";
import { FooterComponent } from './footer/footer.component';


@NgModule({
  declarations: [
    MenuComponent,
    FooterComponent
  ],
    exports: [
        MenuComponent,
        FooterComponent
    ],
  imports: [
    CommonModule,
    RouterModule,
    IconsModule
  ]
})
export class SharedModule { }
