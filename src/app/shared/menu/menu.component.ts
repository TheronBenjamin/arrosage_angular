import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {Observable, tap} from "rxjs";
import {ConnectionService} from "../../connection/service/connection.service";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  public show : boolean = false;
  // public connectedUser$!: Observable<Admin | null>;

  constructor(
    private router: Router,
    private connectionService : ConnectionService,
  // private authService : AuthService
  ) {
  }

  ngOnInit(): void {
    /*this.connectedUser$ = this.authService.getConnectedUser$()
      .pipe(
        tap(value => console.log('Utilisateur connecté: ', value))
      );*/
  }

  public showAndHideMenu(): void {
    this.show = !this.show;
  }

  logout() {
    localStorage.removeItem('currentUserId')
    localStorage.removeItem('currentUserLogin');
    localStorage.removeItem('currentUserMail');
    localStorage.removeItem('Authorization');
    localStorage.removeItem('unloadTime');
    this.router.navigate(['/']);
  }

}
