import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChatRoutingModule } from './chat-routing.module';
import { ChatListComponent } from './pages/chat-list/chat-list.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";


@NgModule({
  declarations: [
    ChatListComponent
  ],
    imports: [
        CommonModule,
        ChatRoutingModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule
    ]
})
export class ChatModule { }
