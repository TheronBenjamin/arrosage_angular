import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Chat} from "../../core/model/chat";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  url: String =  environment.apiURL;
  constructor(private httpClient: HttpClient) { }

  getMessageByEmeteurAndRecepteur(emeteur: number, recepteur: number) : Observable<any> {
    return this.httpClient.get<Chat>(this.url + "messages/getMessageByEmeteurAndRecepteur/" + emeteur + "/" + recepteur)
  }

  getMessageById(id: number) {
    return this.httpClient.get<Chat>(this.url + "messages/getById/" + id)
  }

  createMessage(message: Chat): Observable<Object> {
    return this.httpClient.post(this.url + "messages/", message)
  }

}
