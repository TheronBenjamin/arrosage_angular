import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {Utilisateur} from "../../../core/model/utilisateur";
import {UserService} from "../../../user/service/user.service";
import {ChatService} from "../../service/chat.service";
import {Chat} from "../../../core/model/chat";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-chat-list',
  templateUrl: './chat-list.component.html',
  styleUrls: ['./chat-list.component.scss']
})
export class ChatListComponent implements OnInit {

  public user$?:Observable<Utilisateur[]>;
  currentIdUser: any = sessionStorage.getItem("currentIdUser");
  currentUserLogin: string = '';
  userRecepteurlogin: string = '';
  userRecepteurId: number = 0;
  public messages$?:Observable<Chat[]>;
  jobMessage: Chat = new Chat();
  messageFormgroup: FormGroup;
  sentMessage: string = '';
  currentDate: Date = new Date();

  constructor(
    private userService:UserService,
    private chatService:ChatService
    ) {
    this.messageFormgroup = new FormGroup({
      sentMessage: new FormControl()
    });
  }

  ngOnInit(): void {
    this.user$ = this.userService.getUsers();

    this.userService.getUserById(this.currentIdUser).subscribe(
      (data) => {
        this.currentUserLogin = data.login;
      }
    );

    // this.cdref.detectChanges();

    //  let interv = setInterval(() => {
    //     this.messages$ = this.chatService.getMessageByEmeteurAndRecepteur(this.currentIdUser, this.userRecepteurId);
    // }, 1000)
  }



  openChat(recepteur: number) {
        this.userService.getUserById(recepteur).subscribe(
          (data) => {
            this.userRecepteurlogin = data.login;
            this.userRecepteurId = data.id;
          }
        );

      this.messages$ = this.chatService.getMessageByEmeteurAndRecepteur(this.currentIdUser, recepteur);
  }

  sendMessage() {

    this.jobMessage.dateEnvoi = this.currentDate;
    this.jobMessage.body = this.messageFormgroup.value.sentMessage;
    this.jobMessage.emeteur = this.currentIdUser;
    this.jobMessage.recepteur = this.userRecepteurId;

    this.chatService.createMessage(this.jobMessage).subscribe(
      (data) => {
        this.messageFormgroup.reset();

        this.openChat(this.userRecepteurId);
      }
    );
  }


}
