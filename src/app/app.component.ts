import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'arrosage_angular';

  ngOnInit(): void {
    window.onbeforeunload = function (e) {

      window.localStorage['unloadTime'] = JSON.stringify(new Date());

    };

    window.onload = function () {

      let loadTime = new Date();
      let unloadTime = new Date(JSON.parse(window.localStorage['unloadTime']));
      let refreshTime = loadTime.getTime() - unloadTime.getTime();

      if(refreshTime>3000)//3000 milliseconds
      {
        window.localStorage.removeItem("Authorization");
        window.localStorage.removeItem("currentUserId");
        window.localStorage.removeItem("currentUserLogin");
        window.localStorage.removeItem("currentUserMail");
      }

    };
  }


}
