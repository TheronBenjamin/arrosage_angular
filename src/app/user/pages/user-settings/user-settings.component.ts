import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ConnectionService} from "../../../connection/service/connection.service";
import {UserService} from "../../service/user.service";
import {Router} from "@angular/router";
import {Observable, pipe, tap} from "rxjs";
import {Utilisateur} from "../../../core/model/utilisateur";

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.scss']
})
export class UserSettingsComponent implements OnInit {
  form!: FormGroup;
  errorMessage: string ="";
  // selectedFile: any = null;
  hidePassword: boolean = true;
  hideConfimPassword: boolean = true;
  user!: Utilisateur;


  constructor(
    fb : FormBuilder,
    private userService: UserService,
    private connectionService: ConnectionService,
    private router: Router
  ) {
    this.form = fb.group({
      id: fb.control(localStorage.getItem('currentUserId')),
      mail: fb.control(localStorage.getItem('currentUserMail'), [Validators.required, Validators.email]),
      login: fb.control(localStorage.getItem('currentUserLogin'), [Validators.required]),
      password: fb.control(null, [Validators.required]),
      passwordCheck: fb.control(null, [Validators.required]),
      image1: fb.control(null ),
      image2: fb.control(null),
      image3: fb.control(null),
      botaniste: fb.control(null),
    }, { validator: this.checkPasswords })
  }

  checkPasswords(group: FormGroup) {
    const pass = group.controls['password'].value;
    const confirmPass = group.controls['passwordCheck'].value;
    return pass === confirmPass ? null : { notSame: true };
  }

  ngOnInit(): void {}

  submit() {
    this.userService.updateUser(this.form.value).subscribe({
      next:() => {
        //TODO delete bearer token and redirect to login https://stackoverflow.com/questions/51743047/angular-data-loss-on-reload-using-behaviorsubject
        //TODO Disconnection
        //TODO interface et classe Angular avec ROLE_USER en attribut
        //TODO Page 404 three.js
        //TODO Demander pour les rôles à Benjamin
        //TODO scroll https://stackoverflow.com/questions/48048299/angular-5-scroll-to-top-on-every-route-click
        this.router.navigate(['/']);
      }
    });
  }

  // //Pictures
  // onFileSelected(event : any)
  // {
  //   this.selectedFile = event.target.files[0];
  // }
  //
  // onUpload()
  // {
  //   console.log(this.selectedFile); // You can use FormData upload to backend server
  // }
}
