import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";
import {Utilisateur} from "../../core/model/utilisateur";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  url: String =  environment.apiURL;

  constructor(
    private httpClient: HttpClient,
  ) { }

  updateUser(utilisateur: Utilisateur):Observable<Utilisateur>{
    return this.httpClient.put<Utilisateur>(this.url + "users/" + utilisateur.id, utilisateur);
  }

  getUtilisateur():Observable<Utilisateur>{
      let currentUserMail = localStorage.getItem('currentUserMail');
      return this.httpClient.get<Utilisateur>(this.url + "users/" + currentUserMail)
  }

  getUsers(): Observable<any> {
    return this.httpClient.get(this.url + "users/");
  }

  getUserById(id:number): Observable<Utilisateur> {
    return this.httpClient.get<Utilisateur>(this.url + "users/getUserById/" + id);
  }

}
