export enum typeplante{
  GRASSE =  "Grasse",
  VIVACE = "Vivace",
  CARNIVORE = "Carnivore",
  GOURMANDE = "Gourmande",
}
