import {IUtilisateur} from "../interface/iutilisateur";

export class Utilisateur implements IUtilisateur{
  id:number = 0;
  mail: string = "";
  login: string = "";
  password: string = "";

  constructor(obj?: Partial<Utilisateur>) {
    if (obj) {
      Object.assign(this, obj);
    }
  }

}


