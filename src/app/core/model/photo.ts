import {IPhoto} from "../interface/iPhoto";

export class Photo implements IPhoto{
  id=0;
  descriptif="";
  datePrisePhoto=new Date();
  image: Blob = new Blob()
  planteId=0;
  planteNom="";
  photographeId=0;
  photographeMail="";
  conseilsAvis :string[] = [];
  conseilsIds :number[] = [];

  constructor(obj?: Partial<Photo>) {
    if (obj) {
      Object.assign(this, obj);
    }
  }



}


