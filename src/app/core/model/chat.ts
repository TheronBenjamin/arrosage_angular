import {iChat} from "../interface/iChat";

export class Chat implements iChat {
  id: number = 0;
  dateEnvoi: Date = new Date();
  body: string = "";
  emeteur: number = 0;
  recepteur: number = 0;
}
