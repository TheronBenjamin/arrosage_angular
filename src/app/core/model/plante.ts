import {IPlante} from "../interface/iPlante";
import {Adresse} from "./adresse";

export class Plante implements IPlante{
  id=0;
  adresse= new Adresse();
  categorie="";
  denomination="";
  gardiennageIds=null;
  photosIds=[0];
  coordinate=""

  constructor(obj?: Partial<Plante>) {
    if (obj) {
      Object.assign(this, obj);
    }
  }



}


