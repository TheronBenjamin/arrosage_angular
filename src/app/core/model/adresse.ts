import {IPlante} from "../interface/iPlante";
import {IAdresse} from "../interface/iAdresse";

export class Adresse implements IAdresse{
  id=0;
  private _latitude=48.1113387;
  private _longitude=-1.6800198;

  constructor(obj?: Partial<Adresse>) {
    if (obj) {
      Object.assign(this, obj);
    }
  }


  get latitude(): number {
    return this._latitude;
  }

  set latitude(value: number) {
    this._latitude = value;
  }

  get longitude(): number {
    return this._longitude;
  }

  set longitude(value: number) {
    this._longitude = value;
  }
}


