import {IBearerToken} from "../interface/ibearertoken";

export class BearerToken implements IBearerToken{
  token: string = "";

  constructor(obj?: Partial<BearerToken>) {
    if (obj) {
      Object.assign(this, obj);
    }
  }

}


