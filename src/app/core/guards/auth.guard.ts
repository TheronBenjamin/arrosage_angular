import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {map, Observable, tap} from 'rxjs';
import {ConnectionService} from "../../connection/service/connection.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private connectionService : ConnectionService,
              private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    // this.connectionService.getConnectedUser().subscribe(value => console.log(value))
    // return this.connectionService.getConnectedUser()
    //   .pipe(
    //     map(user => user !== null ? true : this.router.parseUrl('/'))
    //   );
    return localStorage.getItem('currentUserMail') !== null ? true : this.router.parseUrl('/');
  }

}
