import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {

  constructor(
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // This is my helper method to fetch the data from localStorage.
    const token: string | null = localStorage.getItem('Authorization');
    if (request.url.includes(environment.apiURL)) {

      const params = request.params;
      let headers = request.headers;

      if (token !== null && token !== undefined && token.length > 0) {
        headers = headers.set('Authorization', token);
        headers = headers.set('Access-Control-Allow-Origin', '*');
        console.log(headers.get('Authorization'));
        console.log(headers.get('Access-Control-Allow-Origin'));
      }

      request = request.clone({
        params,
        headers
      });
    }

    return next.handle(request);
  }
}
