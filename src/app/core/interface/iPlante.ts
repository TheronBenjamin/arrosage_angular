import {Adresse} from "../model/adresse";

export interface IPlante {
  id:number;
  denomination:string;
  categorie ?:string;
  adresse ?:Adresse;
  gardiennageIds :Array<number>|null;
  photosIds :Array<number>;
}
