
export interface iChat {
  id: number;
  dateEnvoi: Date;
  body: string;
  emeteur: number;
  recepteur: number;
}
