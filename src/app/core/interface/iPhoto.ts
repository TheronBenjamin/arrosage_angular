export interface IPhoto {
  id:number;
  descriptif:string;
  datePrisePhoto: Date;
  image:Blob;
  planteId:number;
  planteNom:string;
  photographeId:number;
  photographeMail:string;
  conseilsAvis :string[];
  conseilsIds :number[];
}
