export interface IUtilisateur {
  id: number;
  mail: string;
  login: string;
  password: string;
}
