import { Component, OnInit } from '@angular/core';
import gsap from "gsap";
import {ConnectionService} from "../../service/connection.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {UserService} from "../../../user/service/user.service";

@Component({
  selector: 'app-connection-page',
  templateUrl: './connection-page.component.html',
  styleUrls: ['./connection-page.component.scss']
})
export class ConnectionPageComponent implements OnInit {

  selectedFile = null;
  form !:FormGroup;
  key: string = "Authorization";
  errorMessage = "";
  hidePassword : boolean = true;

  constructor(
    fb : FormBuilder,
    private connectionService: ConnectionService,
    private userService: UserService,
    private router: Router
  ) {
    this.form = fb.group({
      mail: fb.control(null, [Validators.required, Validators.email]),
      password: fb.control(null, [Validators.required]),
    })
  }

  ngOnInit(): void {
    gsap.from(".example-card",  {duration:'.7', bottom:'120', ease:"power4"})
    gsap.to(".img",  {duration:'1.6',x:'0', rotation:'360', ease:"back"})
    gsap.from(".example-full-width",  {duration:'1.5', opacity:'0', right:'100', ease:"power4"})
    gsap.from(".account-button",  {duration:'1.5', left:'100', ease:"power4"})
    gsap.from(".create-account-text",  {duration:'1.5', x:'100',opacity:'10', ease:"power4"})
    gsap.from(".title",  {duration:'2',y:'-80', opacity:'0', ease:"power4"})
  }

  submit() {
    this.connectionService.logUser(this.form.value).subscribe(
      {
        next: (loginResponse) => {
          localStorage.setItem(this.key, 'Bearer ' + loginResponse.token);
          this.errorMessage = "";
          localStorage.setItem("currentUserMail", this.form.value.mail)
           this.userService.getUtilisateur().subscribe(user => {
            localStorage.setItem("currentUserLogin", user.login)
            localStorage.setItem("currentUserId", String(user.id))
             sessionStorage.setItem("currentIdUser", String(user.id))
            this.router.navigate(['/user']);
           })
        },
        error: error => {
          console.log(error);
          this.errorMessage = "L'adresse mail ou le mot de passe est incorrect"
        }
      });
  }
}
