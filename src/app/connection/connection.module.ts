import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConnectionPageComponent } from './pages/connection-page/connection-page.component';
import {MatCardModule} from "@angular/material/card";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";
import {RouterModule} from "@angular/router";
import {ConnectionRoutingModule} from "./connection-routing.module";
import {ReactiveFormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";

@NgModule({
  declarations: [
    ConnectionPageComponent
  ],
  exports: [
    ConnectionPageComponent
  ],
    imports: [
        CommonModule,
        RouterModule,
        ConnectionRoutingModule,
        MatCardModule,
        MatFormFieldModule,
        MatButtonModule,
        MatInputModule,
        ReactiveFormsModule,
        MatIconModule
    ]
})
export class ConnectionModule { }
