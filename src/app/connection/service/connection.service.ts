import { Injectable } from '@angular/core';
import {Utilisateur} from "../../core/model/utilisateur";
import {BehaviorSubject, Observable, tap} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {environment} from "../../../environments/environment";
import {BearerToken} from "../../core/model/bearerToken";

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {

  private url: string =  environment.apiURL;
  private connectedUser$ = new BehaviorSubject<Utilisateur | null>(null);

  constructor(
    private httpClient : HttpClient,
    private router : Router
  ) { }

  getConnectedUser(): Observable<Utilisateur | null> {
    return this.connectedUser$.asObservable();
  }

  getUtilisateur(utilisateur: Utilisateur):Observable<Utilisateur>{
    return this.httpClient.get<Utilisateur>(this.url + "users/" + utilisateur.mail);
  }

  logUser(utilisateur : Utilisateur):Observable<BearerToken>{
    return this.httpClient.post<BearerToken>(this.url + "users/authenticate", utilisateur)
      .pipe(tap(() => {
        this.getUtilisateur(utilisateur).subscribe(user => {
          this.connectedUser$.next(user);
        })
        this.router.navigate(['/']);
      }));
  }

  logout(){
    return this.httpClient.get(this.url + "logout");
  }

}
