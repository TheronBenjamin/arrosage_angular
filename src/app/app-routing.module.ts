import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MenuComponent} from "./shared/menu/menu.component";
import {AuthGuard} from "./core/guards/auth.guard";



const routes: Routes = [
  {
    path: '', pathMatch:"full", loadChildren: () =>
      import('./connection/connection.module')
        .then((module_) => module_.ConnectionModule)
  },
  {
    path: '', loadChildren: () =>
      import('./inscription/inscription.module')
        .then((module_) => module_.InscriptionModule)
  },
  {
    canActivate: [AuthGuard],
    path: '',
    component: MenuComponent,
    children: [
      {
        path: '', loadChildren: () =>
          import('./user/user.module')
            .then((module_) => module_.UserModule)
      },
      {path: 'plantes', loadChildren: () =>
          import('./plante/plante.module')
            .then((module_)=> module_.PlanteModule)
      },
      {path: 'chat', loadChildren: () =>
          import('./chat/chat.module')
            .then((module_)=> module_.ChatModule)},
    ]
  },
  {path:'**', loadChildren:
      () => import('./page-not-found/page-not-found.module')
        .then((module_) => module_.PageNotFoundModule)
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
