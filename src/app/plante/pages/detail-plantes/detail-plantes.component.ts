import {Component, Input, OnInit} from '@angular/core';
import {PlanteService} from "../../service/plante.service";
import {Plante} from "../../../core/model/plante";
import {ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs";
import {PhotoService} from "../../service/photo.service";
import {Photo} from "../../../core/model/photo";
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";
import {ConnectionService} from "../../../connection/service/connection.service";


@Component({
  selector: 'app-detail-plantes',
  templateUrl: './detail-plantes.component.html',
  styleUrls: ['./detail-plantes.component.scss']
})
export class DetailPlantesComponent implements OnInit {

  // plante:Plante = this.planteService.getPlantesById().subscribe();
  public plante$!: Observable<Plante>;
  planteId:number = 0;
  selectedFile: any = null;
  userMail!: string|null;
  planteName !: string;
  @Input()
  url: string = "";
  urlSafe: SafeResourceUrl | undefined;
  idUtilisateur:number|null |undefined=0;


  constructor(
    private planteService : PlanteService,
    private photoService : PhotoService,
    private activatedRoute: ActivatedRoute,
    public sanitizer: DomSanitizer,
    private connectionService :ConnectionService,
  ) { }

  ngOnInit(): void {
    this.planteId = Number(this.activatedRoute.snapshot.paramMap.get('id'))
    this.plante$ = this.planteService.getPlantesById(this.planteId);
    this.photoService.getPhotos().subscribe(value => console.log(value));
    this.userMail = localStorage.getItem("currentUserMail");
    this.planteService.getPlantesById(this.planteId).subscribe(value => {
      this.planteName = value.denomination
      this.url=`https://www.google.com/maps/embed/v1/place?key=AIzaSyCDrf687CxIuAKUJqlHMvUifHiE8iWQY4U&q=${value.adresse.latitude}%2C${value.adresse.longitude}`
      this.urlSafe= this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
      console.log(value);
    });
    this.connectionService.getConnectedUser().subscribe(value => {
      return this.idUtilisateur = value?.id;
    })

  }

  //Pictures
  onFileSelected(event : any)
  {
    this.selectedFile = event.target.files[0];
  }

  onUpload()
  {
    const formData=new FormData();
    //créer un formulaire
    console.log(this.selectedFile); // You can use FormData upload to backend server
    let photo = new Photo();
    if(this.selectedFile != null){
      photo.image = new File([this.selectedFile], photo.descriptif);
      photo.id= 1;
      photo.planteId = this.planteId;
      photo.descriptif = "test";
      if(this.userMail != null){
        photo.photographeMail = this.userMail;
      } else {
        console.log("Pas de mail");
      }
      //Date to format: yyyy-MM-dd
      let date=photo.datePrisePhoto.toISOString();
      let dateFormat=date.substring(0,10);
      console.log(dateFormat);
      photo.conseilsAvis = ["hello", "world"];
      photo.conseilsIds = [1];
      photo.datePrisePhoto = new Date();
      photo.planteNom = this.planteName;
      console.log(photo);

      formData.append('descriptif',photo.descriptif);
      formData.append('file',photo.image);
      if (this.idUtilisateur!=null){
        formData.append('photographeId',this.idUtilisateur.toString() );
      }

      formData.append('planteId',photo.planteId.toString());
      formData.append('date',dateFormat);
    }
    this.photoService.createPhoto(formData).subscribe(value => console.log(value));
  }
}
