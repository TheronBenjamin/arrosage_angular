import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailPlantesComponent } from './detail-plantes.component';

describe('DetailPlantesComponent', () => {
  let component: DetailPlantesComponent;
  let fixture: ComponentFixture<DetailPlantesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailPlantesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DetailPlantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
