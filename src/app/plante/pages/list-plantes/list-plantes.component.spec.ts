import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPlantesComponent } from './list-plantes.component';

describe('ListPlantesComponent', () => {
  let component: ListPlantesComponent;
  let fixture: ComponentFixture<ListPlantesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListPlantesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListPlantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
