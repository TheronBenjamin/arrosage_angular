import { Component, OnInit } from '@angular/core';
import {PlanteService} from "../../service/plante.service";
import {Plante} from "../../../core/model/plante";
import {Observable} from "rxjs";

@Component({
  selector: 'app-list-plantes',
  templateUrl: './list-plantes.component.html',
  styleUrls: ['./list-plantes.component.scss']
})
export class ListPlantesComponent implements OnInit {
  public plantes$?:Observable<Plante[]> ;
  private event: any;
  private photo: any;

  constructor(private planteService:PlanteService) { }

  ngOnInit(): void {
    this.plantes$ = this.planteService.getPlantes();
  }

  public open(event: any, photo: any) {
    this.event = event;
    this.photo = photo;
    alert(this.planteService.getPhotos())
  }

  submitPhoto() {
    console.log("submitPhoto");
  }
}
