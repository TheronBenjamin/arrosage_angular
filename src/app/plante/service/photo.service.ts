import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Photo} from "../../core/model/photo";

@Injectable({
  providedIn: 'root'
})
export class PhotoService {
  private url: string =  environment.apiURL;

  constructor(private httpClient : HttpClient) {}

  getPhotos():Observable<Photo[]>{
    return this.httpClient.get<Photo[]>(this.url + "photos");
  }

  getPhotosById(id: number):Observable<Photo>{
    return this.httpClient.get<Photo>(this.url + "photos/" + id);
  }

  createPhoto(photo: FormData):Observable<ArrayBuffer>{
    let headers: HttpHeaders = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    console.log(this.url + "photos",photo,{headers});
    return this.httpClient.post<ArrayBuffer>(this.url + "photos",photo,{headers});
  }

  // deletePhoto(id: number):Observable<Photo>{
  //   return this.httpClient.delete<Photo>(this.url + "photos/" + id);
  // }

}
