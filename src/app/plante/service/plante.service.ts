import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Plante} from "../../core/model/plante";

@Injectable({
  providedIn: 'root'
})
export class PlanteService {
  private url: string =  environment.apiURL;

  constructor(private httpClient : HttpClient) {

  }

  getPlantes():Observable<any>{
    return this.httpClient.get(this.url + "plantes");
  }

  getPlantesById(id: number):Observable<Plante>{
    return this.httpClient.get<Plante>(this.url + "plantes/" + id);
  }

  getPhotos():Observable<any>{
    return this.httpClient.get(this.url + "photos");
  }
}
