import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListPlantesComponent} from "./pages/list-plantes/list-plantes.component";
import {DetailPlantesComponent} from "./pages/detail-plantes/detail-plantes.component";

const routes: Routes = [
  {path: '', pathMatch: 'full', component: ListPlantesComponent},
  {path: 'details/:id', component: DetailPlantesComponent}
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanteRoutingModule { }
