import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlanteRoutingModule } from './plante-routing.module';
import { ListPlantesComponent } from './pages/list-plantes/list-plantes.component';
import { DetailPlantesComponent } from './pages/detail-plantes/detail-plantes.component';


@NgModule({
  declarations: [
    ListPlantesComponent,
    DetailPlantesComponent
  ],
  imports: [
    CommonModule,
    PlanteRoutingModule
  ]
})
export class PlanteModule { }
